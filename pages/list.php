<?php

/**
 * Created by PhpStorm.
 * User: barteld.logghe
 * Date: 8-8-2019
 * Time: 15:34
 */
?>
<!DOCTYPE html>
<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">-->
    <!--  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css"> -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/mapsi.css">
    <link rel="stylesheet" type="text/css" href="css/other_table.css">

    <script type="text/javascript" src="javascript/mapsi.js"></script>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>
    <title>Mapsi</title>
    <link rel="shortcut icon" type="image/x-icon" href="https://i.ibb.co/vvDWW1R/logo1.jpg">
</head>
<body class="main_body">


<?php

//get + read config file
if ( !isset( $_SESSION[ 'app' ][ 'homepage' ] ) ) {
	$filepath = __DIR__ . "/../../mapsi_ini/config.ini";
	$config = parse_ini_file( $filepath, true );
	$_SESSION[ 'app' ][ 'homepage' ] = $config[ 'app' ][ 'homepage' ];
}

//destroy session if logged out
if ( $_SESSION[ 'post' ][ 'action' ] == "Log out" ) {
	session_unset();
	session_destroy();

} else {
	if ( $_SESSION[ 'post' ][ 'action' ] == "login" && $_POST ) {
		$params = "&user_email=" . $_SESSION[ 'post' ][ 'user_email' ] . "&user_pass=" . md5( $_SESSION[ 'post' ][ 'password' ] );
		$user = json_decode( $apicall->call_api( $_SESSION[ 'post' ][ 'action' ], "", $params, true ), true );
		$_SESSION[ 'current_return' ] = $user[ "return_status" ];
		if ( $user[ "user" ] ) {
			$_SESSION[ "user" ] = $user[ "user" ];
		} else {
			var_dump( "no user" );
		}
	}

	if ( $_POST ) {
		//First part: post
		//second part: redirect

	} else {
		if ( isset( $_SESSION[ 'post' ][ 'action' ] ) ) {
			//third part: get
			$user = json_decode( $apicall->call_api( "get_user_info", $_SESSION[ 'user' ][ 'user_id' ], "", false ), true );
			$problems = json_decode( $apicall->call_api( "get_problems", $_SESSION[ 'user' ][ 'user_id' ], "", false ), true )[ "problem" ];
			$services = json_decode( $apicall->call_api( "get_services", $_SESSION[ 'user' ][ 'user_id' ], "", false ), true )[ 'services' ];
		}

	}
}
?>



<?php
if ( isset( $_SESSION[ 'user' ][ 'user_id' ] ) ) {

	if ( isset( $_SESSION[ 'post' ][ 'action' ] ) ) {
		$action = $_SESSION[ 'post' ][ 'action' ];
		$list_service="";
		$list_problem="";
		$create_problem="";
		$create_service="";
		$user_info="";
		switch ( $action ) {
			case "modify_service":
			case "create_service":
			case "List Services":
				$list_service="active";
				break;
			case "List Problems":
			case "create_problem":
			case "modify_problem":
			    $list_problem="active";
				break;
			case "Create Problem":
			case "Modify Problem":
			    $create_problem="active";
				break;
			case "Create Service":
			case "Modify Service":
			    $create_service="active";
				break;
			case "User info":
			case "create_user":
			    $user_info="active";
				break;
		}
	}
	/*    <img src="https://img.icons8.com/ios/50/000000/new-contact.png">
    <img src="https://img.icons8.com/cotton/64/000000/settings--v1.png">
    <img src="https://img.icons8.com/carbon-copy/100/000000/question-mark.png">
    <img src="https://img.icons8.com/ios/50/000000/popular-woman.png">*/
	?>

    <nav class="navbar navbar-expand-md navbar-1 navbar-dark bg-primary sticky-top">

        <button class="navbar-toggler" data-toggle="collapse" data-target="#collapse_target">
            <span class="navbar-toggler-icon"></span>
            Dag <?php echo $_SESSION['user']['f_name']?>
        </button>

        <div class="collapse navbar-collapse " id="collapse_target">


            <ul class="navbar-nav nav-pillis">
                <li class="nav-item"
                    onclick="post(<?php echo( "'" . $_SESSION[ 'app' ][ 'homepage' ] . "'" ); ?>,{'action': 'List Services'})">
                    <a class="nav-link <?php echo $list_service?>">Lijst
                        diensten</a></li>
                <li class="nav-item"
                    onclick="post(<?php echo( "'" . $_SESSION[ 'app' ][ 'homepage' ] . "'" ); ?>,{'action': 'List Problems'})">
                    <a class="nav-link <?php echo $list_problem?>">Lijst
                        problemen</a></li>
                <li class="nav-item"
                    onclick="post(<?php echo( "'" . $_SESSION[ 'app' ][ 'homepage' ] . "'" ); ?>,{'action': 'Create Problem'})">
                    <a class="nav-link <?php echo $create_problem?>">Probleem
                        maken</a></li>
                <li class="nav-item"
                    onclick="post(<?php echo( "'" . $_SESSION[ 'app' ][ 'homepage' ] . "'" ); ?>,{'action': 'Create Service'})">
                    <a class="nav-link <?php echo $create_service?>">Dienst
                        maken</a></li>
                <li class="nav-item"
                    onclick="post(<?php echo( "'" . $_SESSION[ 'app' ][ 'homepage' ] . "'" ); ?>,{'action': 'User info'})">
                    <a class="nav-link <?php echo $user_info?>">Gebruikersinfo
                        </a></li>
                <li class="nav-item"
                    onclick="post(<?php echo( "'" . $_SESSION[ 'app' ][ 'homepage' ] . "'" ); ?>,{'action': 'Log out'})">
                    <a class="nav-link">Log
                        out</a></li>
                <li class="nav-item"
                    onclick="post(<?php echo( "'" . $_SESSION[ 'app' ][ 'homepage' ] . "'" ); ?>,{'action': 'Graph2'})">
                    <a class="nav-link">graphs
                        </a></li>
            </ul>
        </div>
    </nav>
	<?php /* old navbar
    <!--Navbar-->
    <nav class="navbar navbar-light navbar-1 white">

        <!-- Navbar brand -->
        <a class="navbar-brand" href="#">Mapsi</a>

        <!-- Collapse button -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent15"
                aria-controls="navbarSupportedContent15" aria-expanded="false" aria-label="Toggle navigation"><span
                    class="navbar-toggler-icon"></span></button>

        <!-- Collapsible content -->
        <div class="collapse navbar-collapse" id="navbarSupportedContent15">

            <!-- Links -->
            <ul class="navbar-nav mr-auto">
                <li class="nav-item"
                    onclick="post(<?php echo( "'" . $_SESSION[ 'app' ][ 'homepage' ] . "'" ); ?>,{'action': 'List Services'})">
                    <a class="nav-link">List
                        Services</a></li>
                <li class="nav-item"
                    onclick="post(<?php echo( "'" . $_SESSION[ 'app' ][ 'homepage' ] . "'" ); ?>,{'action': 'List Problems'})">
                    <a class="nav-link">List
                        Problems</a></li>
                <li class="nav-item"
                    onclick="post(<?php echo( "'" . $_SESSION[ 'app' ][ 'homepage' ] . "'" ); ?>,{'action': 'Create Problem'})">
                    <a class="nav-link">Create
                        Problem</a></li>
                <li class="nav-item"
                    onclick="post(<?php echo( "'" . $_SESSION[ 'app' ][ 'homepage' ] . "'" ); ?>,{'action': 'Create Service'})">
                    <a class="nav-link">Create
                        Service</a></li>
                <li class="nav-item"
                    onclick="post(<?php echo( "'" . $_SESSION[ 'app' ][ 'homepage' ] . "'" ); ?>,{'action': 'User info'})">
                    <a class="nav-link">User
                        info</a></li>
                <li class="nav-item"
                    onclick="post(<?php echo( "'" . $_SESSION[ 'app' ][ 'homepage' ] . "'" ); ?>,{'action': 'Log out'})">
                    <a class="nav-link">Log
                        out</a></li>
            </ul>
            <!-- Links -->

        </div>
        <!-- Collapsible content -->

    </nav>
    <!--/.Navbar--> */ ?>
	<?php
}
if ( isset( $_SESSION[ 'return' ] ) ) {

	foreach ( $_SESSION[ 'return' ] as $key => $action ) {
		unset( $class );
		if ( isset( $action ) ) {
			switch ( $action[ 'return code' ] ) {
				case 1:
					$class = "alert alert-warning";
					break;
				case 2:
					$class = "alert alert-danger";
					break;
				case 0:
					$class = "alert alert-success";
					break;
			}
			if ( isset( $class ) ) {
				?>
                <div class="<?php echo $class ?>">
					<?php echo $action[ 'msg' ] ?>.
                </div>
				<?php
			}
		}

	}
}
//var_dump($_SESSION);

?>
<div class="container">
		<?php
		if($_SESSION['post']['action']=="User info" ||$_SESSION['post']['action']=="create_user" || $_SESSION['post']['action']=="login") {
			if ( !empty( $_SESSION[ "user" ] ) ) {

				?>
                <div class="table-responsive-sm">
                    <table class="table  border="1">
                    <thead class="thead-dark" align="left">
                    <tr>
                        <th scope="col" hidden="hidden">gebruiker id</th>
                        <th scope="col">voornaam</th>
                        <th scope="col">achternaam</th>
                        <th scope="col">email</th>
                    </tr>
                    </thead>
                    <tr>
                        <td hidden="hidden"><?php echo( $_SESSION   [ 'user' ][ 'user_id' ] ); ?></td>
                        <td><?php echo( $_SESSION[ 'user' ][ 'f_name' ] ); ?></td>
                        <td><?php echo( $_SESSION[ 'user' ][ 'l_name' ] ); ?></td>
                        <td><?php echo( $_SESSION[ 'user' ][ 'user_email' ] ); ?></td>
                    </tr>
                    </table>
                </div>
				<?php
			}
		}
		?>



