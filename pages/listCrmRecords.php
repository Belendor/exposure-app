<?php

/**
 * Created by PhpStorm.
 * User: barteld.logghe
 * Date: 8-8-2019
 * Time: 14:19
 */
/**
 * @var$apicall apicall
 */
$apicall = new apicall();
if ( $_POST[ 'action' ] == "create_service" ) {

	$params = "&service_description=" . urlencode( $_POST[ 'service_description' ] ) . "&service_email=" . urlencode( $_POST[ 'service_email' ] );
	$new_service = json_decode( $apicall->call_api( $_POST[ 'action' ], $_SESSION[ 'user' ][ 'user_id' ], $params ), true );
}
if ( $_POST[ 'action' ] == "modify_service" ) {
	//var_dump( "modify van service" );
	$params = "&service_id=" . urlencode( $_POST[ 'service_id' ] ) . "&service_description=" . urlencode( $_POST[ 'service_description' ] ) . "&service_email=" . urlencode( $_POST[ 'service_email' ] );
	$new_service = json_decode( $apicall->call_api( $_POST[ 'action' ], $_SESSION[ 'user' ][ 'user_id' ], $params ), true );
}
$services = json_decode( $apicall->call_api( "get_services", $_SESSION[ 'user' ][ 'user_id' ], "" ), true )[ 'services' ][ 'services' ];

?>
<?php
if ( $services ) {

	?>
    <form>
        <div class="form-row">

            <div class="form-group" class="col-md-2">
                <label for="reporter_email" class="text-info">zoek service:</label><br>
                <input type="text" name="serviceNameFilter" onkeyup="filterService()" id="serviceNameFilter"
                       class="form-control">
            </div>
        </div>
    </form>
    <div class="table-responsive-sm">

    <table class="table" style="width:100%" border="1" id="serviceTable">
    <thead class="thead-dark" align="left">
    <tr>
        <th hidden="hidden" scope="col">dienst id</th>
        <th scope="col" onclick="sortTable(1,'serviceTable')">dienst omschrijving</th>
        <th scope="col" onclick="sortTable(1,'serviceTable')">dienst email</th>
    </tr>
    </thead>
	<?php
	foreach ( $services as $key => $service ) {
		?>
        <form action=<?php echo( "'" . $_SESSION[ 'app' ][ 'homepage' ] . "'" ); ?> method="post">
            <tr scope="row" onclick="
                    post(<?php echo( "'" . $_SESSION[ 'app' ][ 'homepage' ] . "'" ); ?>,{'action': 'Modify Service',
                    'user_id' :'<?php echo( $user[ 'user' ][ 'user_id' ] ); ?>',
                    'service_id':'<?php echo( $service[ 'service_id' ] ); ?>',
                    'service_description':'<?php echo( $service[ 'service_description' ] ); ?>',
                    'service_email':'<?php echo( $service[ 'service_email' ] ); ?>'
                    })">
                <td hidden="hidden"><?php echo( $service[ 'service_id' ] ); ?></td>
                <td><?php echo( $service[ 'service_description' ] ); ?></td>
                <td><?php echo( $service[ 'service_email' ] ); ?></td>
            </tr>
            <input type="hidden" name="user_id" value="<?php echo( $user[ 'user' ][ 'user_id' ] ); ?>"/>
            <input type="hidden" name="service_id" value="<?php echo( $service[ 'service_id' ] ); ?>"/>
            <input type="hidden" name="service_description"
                   value="<?php echo( $service[ 'service_description' ] ); ?>"/>
            <input type="hidden" name="service_email" value="<?php echo( $service[ 'service_email' ] ); ?>"/>
        </form>
		<?php
	}
} else {
	?>
    no services found
	<?php
}
?>
    </table>
    </div>


<?php
?>