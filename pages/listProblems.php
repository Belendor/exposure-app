<?php

/**
 * Created by PhpStorm.
 * User: barteld.logghe
 * Date: 8-8-2019
 * Time: 14:20
 */

/**
 * @var$apicall apicall
 */
$apicall = new apicall();
if ( $_POST[ 'action' ] == "create_problem" ) {
	$params = "&service_id=" . urlencode( $_POST[ 'service_id' ] ) . "&reporter_email=" . urlencode( $_POST[ 'reporter_email' ] ) . "&reporter_f_name=" . urlencode( $_POST[ 'reporter_f_name' ] ) . "&reporter_l_name=" . urlencode( $_POST[ 'reporter_l_name' ] ) . "&problem_description=" . urlencode( $_POST[ 'problem_description' ] );

	$new_problem = json_decode( $apicall->call_api( $_POST[ 'action' ], $_POST[ 'user_id' ], $params, true ), true );
	$_SESSION[ 'current_return' ] = $new_problem[ "return_status" ];

}
if ( $_POST[ 'action' ] == "modify_problem" ) {
	$params = "&problem_id=" . urlencode( $_POST[ 'problem_id' ] ) . "&service_id=" . urlencode( $_POST[ 'service_id' ] ) . "&reporter_email=" . urlencode( $_POST[ 'reporter_email' ] ) . "&reporter_f_name=" . urlencode( $_POST[ 'reporter_f_name' ] ) . "&reporter_l_name=" . urlencode( $_POST[ 'reporter_l_name' ] ) . "&problem_description=" . urlencode( $_POST[ 'problem_description' ] );
	$_SESSION[ 'params' ] = $params;
	$_SESSION[ 'description' ] = $_POST[ 'problem_description' ];
	$_SESSION[ 'urlencode' ] = urlencode( $_POST[ 'problem_description' ] );
	$_SESSION[ 'htmlentities' ] = htmlentities( $_POST[ 'problem_description' ] );

	$new_problem = json_decode( $apicall->call_api( $_POST[ 'action' ], $_POST[ 'user_id' ], $params, true ), true );
	$_SESSION[ 'current_return' ] = $new_problem[ "return_status" ];
}
$problems = json_decode( $apicall->call_api( "get_problems", $_SESSION[ 'user' ][ 'user_id' ], "", false ), true )[ "problem" ];

?>

<?php
if ( $problems) {

	?>
    <form>

        <div class="form-row">
            <div class="form-group col-md-2">
                <label for="selectService" class="badge badge-primary">Dienst</label>
                <select id="selectService" class="form-control" onchange="problemFilter()">
                    <option value="" selected="selected"></option>
					<?php
					foreach ( $services as $key => $service ) {
						?>
                        <option
                                value="<?php echo( $service[ 'service_id' ] ); ?>"><?php echo( $service[ 'service_description' ] ); ?></option>
						<?php
					}
					?>
                </select>
            </div>
            <div class="form-group col-md-2">
                <label for="selectStatus" class="badge badge-primary">Status</label>
                <select id="selectStatus" class="form-control" onchange="problemFilter()">
                    <option value="" selected="selected"></option>
                    <option value="0">ongoing</option>
                    <option value="1">to be confirmed</option>
                    <option value="2">confirmed</option>
                    <option value="3">failed</option>
                    <option value="4">solved</option>
                </select>
            </div>
        </div>

    </form>




    <div id="data" class="jim-table-responsive">
    <table class="table" id="problemTable">
    <thead class="thead-dark" align="left">
    <tr>
        <th hidden="hidden">probleem id</th>
        <th onclick="sortTable(3,'problemTable')">Probleem omschrijving</th>
        <th onclick="sortTable(1,'problemTable')">Probleem status</th>
        <th onclick="sortTable(5,'problemTable')">Stadsdienst</th>
        <th onclick="sortTable(6,'problemTable')">Email reporter</th>
    </tr>
    </thead>
	<?php
	//var_dump($problems);

	foreach ( $problems as $key => $problem ) {
		$status = "";
		$color = "";
		switch ( $problem[ 'problem_status' ] ) {
			case 0:
				$status_description = "ongoing";
				$color = "#D0FA58";
				$colorScheme = "table-warning";

				break;
			case 1:
				$status_description = "to be confirmed";
				$color = "#A9D0F5";
				$colorScheme = "table-primary";

				break;
			case 2:
				$status_description = "confirmed";
				$color = "#5882FA";
				$colorScheme = "table-info";

				break;
			case 3:
				$status_description = "failed";
				$color = "#FA5858";
				$colorScheme = "table-danger";

				break;
			case 4:
				$status_description = "solved";
				$color = "#81F781";
				$colorScheme = "table-success";

				break;
			default:
				break;
		}
		?>
        <form action=<?php echo( "'" . $_SESSION[ 'app' ][ 'homepage' ] . "'" ); ?> method="post">

            <tr class="<?php echo $colorScheme ?>" onclick=
            "post(<?php echo( "'" . $_SESSION[ 'app' ][ 'homepage' ] . "'" ); ?>,{'action': 'Modify Problem',
                    'user_id' :'<?php echo( $_SESSION[ 'user' ][ 'user_id' ] ); ?>',
                    'service_id':'<?php echo( $problem[ 'service_id' ] ); ?>',
                    'problem_id':'<?php echo( $problem[ 'problem_id' ] ); ?>',
                    'reporter_f_name':'<?php echo( $problem[ 'reporter_f_name' ] ); ?>',
                    'reporter_l_name':'<?php echo( $problem[ 'reporter_l_name' ] ); ?>',
                    'problem_description':'<?php echo( htmlspecialchars( json_encode( $problem[ 'problem_description' ] ) ) ); ?>',
                    'reporter_email':'<?php echo( $problem[ 'reporter_email' ] ); ?>'
                    })">
                <td hidden="hidden"><?php echo( $problem[ 'service_id' ] ); ?></td>
                <td hidden="hidden"><?php echo( $problem[ 'problem_status' ] ); ?></td>
                <td hidden="hidden"><?php echo( $problem[ 'problem_id' ] ); ?></td>
                <td data-label="Omschrijving"><?php echo( $problem[ 'problem_description' ] ); ?></td>
                <td data-label="Status"><?php echo( $status_description ); ?></td>
                <td data-label="Dienst"><?php echo( $problem[ 'service_email' ] ); ?></td>
                <td data-label="Reporter"><?php echo( $problem[ 'reporter_email' ] ); ?></td>
            </tr>
            <input type="hidden" name="problem_id" value="<?php echo( $problem[ 'problem_id' ] ); ?>"/>
            <input type="hidden" name="user_id" value="<?php echo( $user[ 'user' ][ 'user_id' ] ); ?>"/>
            <input type="hidden" name="service_id" value="<?php echo( $problem[ 'service_id' ] ); ?>"/>
            <input type="hidden" name="reporter_f_name" value="<?php echo( $problem[ 'reporter_f_name' ] ); ?>"/>
            <input type="hidden" name="reporter_l_name" value="<?php echo( $problem[ 'reporter_l_name' ] ); ?>"/>
            <input type="hidden" name="problem_description"
                   value="<?php echo( $problem[ 'problem_description' ] ); ?>"/>
            <input type="hidden" name="reporter_email" value="<?php echo( $problem[ 'reporter_email' ] ); ?>"/>
        </form>
		<?php
	}
	?>
    </table>
    </div>
    <?php

} else {
	?>
    no problems found.
    <div id="data" class="jim-table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th>Country</th>
                <th>Official Name</th>
                <th>Official Language</th>
                <th>Capital</th>
                <th>Currency</th>
                <th>Population</th>
                <th>Size/Area</th>
                <th>Flag</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td data-label="Country">Germany</td>
                <td data-label="Official Name">Federal Republic of Germany</td>
                <td data-label="Official Language">German</td>
                <td data-label="Capital">Berlin</td>
                <td data-label="Currency">Euro</td>
                <td data-label="Population">80,854,408</td>
                <td data-label="Size/Area">357,022 sq km</td>
                <td data-label="Flag"><img src="https://www.cia.gov/library/publications/the-world-factbook/graphics/flags/large/gm-lgflag.gif" height="32"/></td>
            </tr>
            <tr>
                <td data-label="Country">Ghana</td>
                <td data-label="Official Name">Republic of Ghana</td>
                <td data-label="Official Language">Asante, Ewe, Fante, other</td>
                <td data-label="Capital">Accra</td>
                <td data-label="Currency">Ghana cedi</td>
                <td data-label="Population">26,327,649</td>
                <td data-label="Size/Area">238,533 sq km</td>
                <td data-label="Flag"><img src="https://www.cia.gov/library/publications/the-world-factbook/graphics/flags/large/gh-lgflag.gif" height="32"/></td>
            </tr>
            <tr>
                <td data-label="Country">Greece</td>
                <td data-label="Official Name">Hellenic Republic</td>
                <td data-label="Official Language">Greek</td>
                <td data-label="Capital">Athens</td>
                <td data-label="Currency">Euro</td>
                <td data-label="Population">10,775,643</td>
                <td data-label="Size/Area">131,957 sq km</td>
                <td data-label="Flag"><img src="https://www.cia.gov/library/publications/the-world-factbook/graphics/flags/large/gr-lgflag.gif" height="32"/></td>
            </tr>
            <tr>
                <td data-label="Country">Greenland</td>
                <td data-label="Official Name">Greenland</td>
                <td data-label="Official Language">Greenlandic, Danish, English</td>
                <td data-label="Capital">Nuuk</td>
                <td data-label="Currency">Danish krone</td>
                <td data-label="Population">57,733</td>
                <td data-label="Size/Area">2,166,086 sq km</td>
                <td data-label="Flag"><img src="https://www.cia.gov/library/publications/the-world-factbook/graphics/flags/large/gl-lgflag.gif" height="32"/></td>
            </tr>
            <tr>
                <td data-label="Country">Guatemala</td>
                <td data-label="Official Name">Republic of Guatemala</td>
                <td data-label="Official Language">Spanish</td>
                <td data-label="Capital">Guatemala City</td>
                <td data-label="Currency">Quetzal</td>
                <td data-label="Population">14,918,999</td>
                <td data-label="Size/Area">108,889 sq km</td>
                <td data-label="Flag"><img src="https://www.cia.gov/library/publications/the-world-factbook/graphics/flags/large/gt-lgflag.gif" height="32"/></td>
            </tr>
            </tbody>
        </table>
    </div>

	<?php
}
?>
    </table>
    </div>

    <div id="status">

    </div>
<?php
?>