<?php
/**
 * Created by PhpStorm.
 * User: barteld.logghe
 * Date: 23-9-2019
 * Time: 9:03
 */
if ( isset( $problems ) && !empty( $problems ) ) {

	$ongoing = 0;
	$confirmed = 0;
	$toBeConfirmed = 0;
	$failed = 0;
	$solved = 0;
	$serviceCount = array();
	$statusCount = array();
	$service_description = array();
	$colorScheme = array(
		0 => 'rgba(255, 99, 132, 0.2)',
		1 => 'rgba(54, 162, 235, 0.2)',
		2 => 'rgba(255, 206, 86, 0.2)',
		3 => 'rgba(75, 192, 192, 0.2)',
		4 => 'rgba(153, 102, 255, 0.2)',
		5 => 'rgba(255, 159, 64, 1)',
	);

	foreach ( $problems as $problem ) {
		@$serviceCount[ $problem[ 'service_id' ] ]++;
		@$statusCount[ $problem[ 'problem_status' ] ]++;
	}
	//var_dump( $statusCount );
	//var_dump( $serviceCount );

	foreach ( $serviceCount as $service_id => $amount ) {

		if ( $service_id === key( $serviceCount ) )
		    $a=1;
			//echo $service_id;
		else
		    $b=1;
			//echo $service_id . ', ';
	}
	foreach ( $serviceCount as $service_id => $amount ) {
		$service_description[ $service_id ] = $services[ array_search( $service_id, array_column( $services, 'service_id' ) ) ][ 'service_description' ];
	}

	$i = 0;
	//var_dump( $serviceCount );
	//var_dump( $service_description[ '0266a49e-c40a-11e9-a071-0050568c3089' ] );
	//var_dump( array_keys( $service_description ) );
	//var_dump( array_search( '0266a49e-c40a-11e9-a071-0050568c3089', array_keys( $service_description ) ) );
	//var_dump( array_keys( array_keys( $service_description ) ) );

	foreach ( $service_description as $service_id => $description ) {

		end( $service_description );
		if ( $service_id === key( $service_description ) )
		    $b=1;
			//echo '"' . $description . '"';
			//var_dump( array_search( $service_id, array_keys( $service_description ) ) );
		else
		    $a=1;
			//var_dump( array_search( $service_id, array_keys( $service_description ) ) );

		//echo '"' . $description . '",';
	}
	?>

    <form>

        <div class="form-row">
            <div class="form-group col-md-2">
                <label for="graphService" class="badge badge-primary">Dienst</label>
                <select id="graphService" class="form-control" onchange="graphFilterService()">
                    <option value="" selected="selected"></option>
					<?php
					foreach ( $services as $key => $service ) {
						?>
                        <option
                                value="<?php echo( $service[ 'service_id' ] ); ?>"><?php echo( $service[ 'service_description' ] ); ?></option>
						<?php
					}
					?>
                </select>
            </div>
        </div>
    </form>

    <canvas id="myChart"></canvas>
    <form>
        <div class="form-row">

            <div class="form-group col-md-2">
                <label for="graphStatus" class="badge badge-primary">Status</label>
                <select id="graphStatus" class="form-control" onchange="graphFilterStatus()">
                    <option value="" selected="selected"></option>
                    <option value="0">ongoing</option>
                    <option value="1">to be confirmed</option>
                    <option value="2">confirmed</option>
                    <option value="3">failed</option>
                    <option value="4">solved</option>
                </select>
            </div>
        </div>
    </form>
    <canvas id="myChart2"></canvas>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
    <script>

		let statusGraphConf;
		let serviceGraphConf;


		function graphFilterService() {
			//todo clear graph
			//todo check values of current filter
			myChart.clear();
			//alert( 'in graph service' );
			let serviceFilter = document.getElementById( "graphService" );
			let serviceId = serviceFilter.options[ serviceFilter.selectedIndex ].value;
			//alert( serviceId );
			if ( serviceId === '' ) {
				//todo get default plot -> maybe declare as conf
			} else {
				var problems = <?php echo json_encode( $problems ) ?>;
				//alert( problems );
				for ( var i = 0; i < problems.length; i++ ) {
					if ( problems[ i ][ 'service_id' ] === serviceId ) {
						//alert(problems[i]);
					} else {
						problems.splice( i, 1 );
						i--;
					}
				}
				//alert( problems );
				myChart.data.datasets[ 0 ].data[ 0 ] = getStatusbyServiceCount( problems, 0 );
				myChart.data.datasets[ 0 ].data[ 1 ] = getStatusbyServiceCount( problems, 1 );
				myChart.data.datasets[ 0 ].data[ 2 ] = getStatusbyServiceCount( problems, 2 );
				myChart.data.datasets[ 0 ].data[ 3 ] = getStatusbyServiceCount( problems, 3 );
				myChart.data.datasets[ 0 ].data[ 4 ] = getStatusbyServiceCount( problems, 4 );
				myChart.update();
			}
		}


		function graphFilterStatus() {
			//myChart2.clear();
			let labels=Array();
			let data=Array();
			let colors=Array();
			let bgcolors=Array();
			//todo clear graph
			//todo check values of current filter
			//alert( 'in graph status' );
			let statusFilter = document.getElementById( "graphStatus" );
			let statusId = statusFilter.options[ statusFilter.selectedIndex ].value;
			//todo if statudId = get default values -> config values -> maybe declare as conf var.
			if ( statusId == '' ) {
				//todo redraw graph with default values
			} else {
				var problems = <?php echo json_encode( $problems ) ?>;
				var services = <?php echo json_encode( $services ) ?>;
				//todo get list of all problems with status = filter
				for ( var i = 0; i < problems.length; i++ ) {
					if ( problems[ i ][ 'problem_status' ] == statusId ) {
					} else {
						problems.splice( i, 1 );
						i--;
					}
				}
				//todo for each service count howmany we have of this status.
				for ( var i = 0; i < services.length; i++ ) {
					newValue = getServicebyStatusCount( problems, services[ i ].service_id );
					if ( newValue > 0 ) {
						labels.push(services[ i ].service_description);
						data.push(newValue);
						colors.push('rgba(255, 99, 132, 0.2)');
						bgcolors.push('rgba(255, 99, 132, 0.2)');

					}
				}
				//alert (labels);

				//redraw
				//alert( labels + " :" + data + " : " + colors + " : " + bgcolors );
				graphFilterStatus2( labels, data, colors, bgcolors );
			}
		}

		function graphFilterStatus2( newLabels, newData, newColors, newBgcolors ) {
			myChart2.destroy();
			//myChart2.removeData();
			//myChart2.datasets.labels.pop();
			//alert( newData );
			//newData = [ 10, 2, 1, 3, 1, 1, 8, 5 ];
			//alert( newData );

			//newColors = [ 'rgba(255, 99, 132, 0.2)', 'rgba(255, 99, 132, 0.2)', 'rgba(255, 99, 132, 0.2)', 'rgba(255, 99, 132, 0.2)', 'rgba(255, 99, 132, 0.2)', 'rgba(255, 99, 132, 0.2)', 'rgba(255, 99, 132, 0.2)', 'rgba(255, 99, 132, 0.2)' ];
			//newBgcolors = [ 'rgba(255, 99, 132, 0.2)', 'rgba(255, 99, 132, 0.2)', 'rgba(255, 99, 132, 0.2)', 'rgba(255, 99, 132, 0.2)', 'rgba(255, 99, 132, 0.2)', 'rgba(255, 99, 132, 0.2)', 'rgba(255, 99, 132, 0.2)', 'rgba(255, 99, 132, 0.2)' ];
			//newLabels = [ 'Dienstchangedbbbbbbbb', 'dienst3', 'groendienst', 'newlayoutservice', 'iets', 'kakakkakakaka', 'dsddsfaaaa', 'bartelddienst2' ];
			//removeData(myChart2);
			//todo clear graph
			//todo check values of current filter
			//alert( 'in graph status' );
			var ctx = document.getElementById( 'myChart2' ).getContext( '2d' );

			myChart2 = new Chart( ctx, {
				type: 'doughnut',
				data: {
					brol: [ 'kaka', 'pipi' ],
					labels: newLabels,
					datasets: [ {
						label: '# of Votes',
						data: newData,
						backgroundColor: newColors,
						borderColor: newColors,
						borderWidth: 1
					} ]
				},
				options: {}
			} );
			//alert( "hier" );
			myChart2.update();

		}


		function getServicebyStatusCount( arr, serviceId ) {
			var count = 0;
			//alert(serviceId);
			for ( var i = 0; i < arr.length; i++ ) {
				//alert(serviceId + " " + i +" " + " " + arr[i].service_id);
				if ( arr[ i ].service_id == serviceId ) {
					count++;
				}
			}
			return count;
		}

		function getStatusbyServiceCount( arr, status ) {
			var count = 0;
			for ( var i = 0; i < arr.length; i++ ) {
				if ( arr[ i ].problem_status == status ) {
					count++;
				}
			}
			return count;
		}

		var ctx = document.getElementById( 'myChart' ).getContext( '2d' );
		var myChart = new Chart( ctx, {
			type: 'doughnut',
			data: {
				labels: [ 'Ongoing', 'To be Confirmed', 'Confirmed', 'Failed', 'Solved' ],
				datasets: [ {
					label: 'Status of problems',
					data: [ <?php echo $statusCount[ 0 ]?>, <?php echo $statusCount[ 1 ]?>, <?php echo $statusCount[ 2 ]?>, <?php echo $statusCount[ 3 ]?>, <?php echo $statusCount[ 4 ]?> ],
					backgroundColor: [
						'rgba(255, 99, 132, 0.2)',
						'rgba(54, 162, 235, 0.2)',
						'rgba(255, 206, 86, 0.2)',
						'rgba(75, 192, 192, 0.2)',
						'rgba(153, 102, 255, 0.2)'
					],
					borderColor: [
						'rgba(255, 99, 132, 1)',
						'rgba(54, 162, 235, 1)',
						'rgba(255, 206, 86, 1)',
						'rgba(75, 192, 192, 1)',
						'rgba(153, 102, 255, 1)'
					],
					borderWidth: 1
				} ]
			},
			options: {}
		} );

		var ctx = document.getElementById( 'myChart2' ).getContext( '2d' );
		var myChart2 = new Chart( ctx, {
			type: 'doughnut',
			data: {
				labels: [ <?php
					//TODO change to foreach servicecount to prevent 0 values from being added.
					foreach ( $service_description as $service_id => $description ) {

						end( $service_description );
						if ( $service_id === key( $service_description ) )
							echo '"' . $description . '"';
						else
							echo '"' . $description . '",';
					}
					?>
				],
				datasets: [ {
					label: '# of Votes',
					data: [ <?php
						foreach ( $serviceCount as $service_id => $amount ) {

							end( $serviceCount );
							if ( $service_id === key( $serviceCount ) )
								echo $amount;
							else
								echo $amount . ",";
						}
						?>
					],
					backgroundColor: [<?php
						$i = 0;
						foreach ( $serviceCount as $service_id => $amount ) {
							end( $colorScheme );
							if ( $i === key( $colorScheme ) ) {
								$i = 0;
							} else {
								$i++;
							}
							end( $serviceCount );
							if ( $service_id === key( $serviceCount ) )
								echo "'" . $colorScheme[ $i ] . "'";
							else
								echo "'" . $colorScheme[ $i ] . "',";
						}?>

					],
					borderColor: [<?php
						$i = 0;
						foreach ( $serviceCount as $service_id => $amount ) {
							end( $colorScheme );
							if ( $i === key( $colorScheme ) ) {
								$i = 0;
							} else {
								$i++;
							}
							end( $serviceCount );
							if ( $service_id === key( $serviceCount ) )
								echo "'" . $colorScheme[ $i ] . "'";
							else
								echo "'" . $colorScheme[ $i ] . "',";
						}?>
					],
					borderWidth: 1
				} ]
			},
			options: {}
		} );

    </script>

	<?php
} else {
	?>
    no graphs to show
	<?php
}