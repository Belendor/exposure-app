<?php
/**
 * Created by PhpStorm.
 * User: barteld.logghe
 * Date: 8-8-2019
 * Time: 14:19
 */
?>
<?php
if ( $_SESSION[ 'post' ][ 'action' ] == "Modify Service" ) {
	?>

    <div class="container">
        <div id="problem-row" class="row justify-content-center align-items-center">
            <div id="problem-column" class="col-md-6">
                <div id="problem-box" class="col-md-12">
                    <form id="problem-form" class="form"
                          action=<?php echo( "'" . $_SESSION[ 'app' ][ 'homepage' ] . "'" ); ?> method="post">
                        <h3 class="text-center text-primary">Pas een Dienst aan</h3>


                        <div class="form-group">
                            <label for="service_description" class="text-primary">Dienst omschrijving</label><br>
                            <input type="text" name="service_description" id="service_description" class="form-control"
                                   value="<?php echo( $_SESSION[ 'post' ][ 'service_description' ] ); ?>">
                        </div>

                        <div class="form-group">
                            <label for="service_email" class="text-primary">Email dienst:</label><br>
                            <input type="email" name="service_email" id="service_email" class="form-control"
                                   value="<?php echo( $_SESSION[ 'post' ][ 'service_email' ] ); ?>">
                        </div>

                        <div class="form-group">
                            <input type="hidden" name="user_id"
                                   value="<?php echo( $_SESSION[ 'post' ][ 'user_id' ] ); ?>"/>
                        </div>

                        <div class="form-group">
                            <input type="hidden" name="service_id"
                                   value="<?php echo( $_SESSION[ 'post' ][ 'service_id' ] ); ?>"/>
                        </div>

                        <div class="form-group">
                            <button type="submit" name="action" class="btn btn-primary btn-md rounded-pill" value="modify_service">Opslaan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

	<?php
} else {
	?>

    <div class="container">
        <div id="problem-row" class="row justify-content-center align-items-center">
            <div id="problem-column" class="col-md-6">
                <div id="problem-box" class="col-md-12">
                    <form id="problem-form" class="form"
                          action=<?php echo( "'" . $_SESSION[ 'app' ][ 'homepage' ] . "'" ); ?> method="post">
                        <h3 class="text-center text-primary">Dienst maken</h3>

                        <div class="form-group">
                            <label for="service_description" class="text-primary">Dienst omschrijving</label><br>
                            <input type="text" name="service_description" id="service_description" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="service_email" class="text-primary">Email dienst:</label><br>
                            <input type="email" name="service_email" id="service_email" class="form-control">
                        </div>

                        <div class="form-group">
                            <input type="hidden" name="user_id" value="<?php echo( $user[ 'user' ][ 'user_id' ] ); ?>"/>
                        </div>

                        <div class="form-group">
                            <button type="submit" name="action" class="btn btn-primary btn-md rounded-pill" value="create_service">Opslaan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

	<?php
}
?>