<?php
/**
 * Created by PhpStorm.
 * User: barteld.logghe
 * Date: 8-8-2019
 * Time: 14:20
 */

if ( !isset( $services ) ) {
	?>
    <div class="alert alert-warning">
        No services created yet. Please create a service before adding any problems.
    </div>
	<?php
	die( "" );
} else {
	//var_dump($_SESSION);
	//var_dump($_SESSION[ 'post' ][ 'service_id' ]);
	$service_index = array_search( $_SESSION[ 'post' ][ 'service_id' ], array_column( $services, "service_id" ) );

	if ( $_SESSION[ 'post' ][ 'action' ] == "Modify Problem" ) {
		?>

        <div class="container">
            <div id="problem-row" class="row justify-content-center align-items-center">
                <div id="problem-column" class="col-md-6">
                    <div id="problem-box" class="col-md-12">
                        <form id="problem-form" class="form"
                              action=<?php echo( "'" . $_SESSION[ 'app' ][ 'homepage' ] . "'" ); ?> method="post">
                            <h3 class="text-center text-primary">Pas een probleem aan</h3>


                            <div class="form-group">
                                <label for="reporter_email" class="text-primary">Email reporter:</label><br>
                                <input type="email" name="reporter_email" id="reporter_email" class="form-control"
                                       value="<?php echo( $_SESSION[ 'post' ][ 'reporter_email' ] ); ?>">
                            </div>

                            <div class="form-group">
                                <label for="reporter_f_name" class="text-primary">Voornaam reporter:</label><br>
                                <input type="text" name="reporter_f_name" id="reporter_f_name" class="form-control"
                                       value="<?php echo( $_SESSION[ 'post' ][ 'reporter_f_name' ] ); ?>">
                            </div>

                            <div class="form-group">
                                <label for="reporter_l_name" class="text-primary">Achternaam reporter:</label><br>
                                <input type="text" name="reporter_l_name" id="reporter_l_name" class="form-control"
                                       value="<?php echo( $_SESSION[ 'post' ][ 'reporter_l_name' ] ); ?>">
                            </div>

                            <div class="form-group">
                                <label for="service_id" class="text-primary">Stadsdienst:</label>
                                <select name="service_id" class="form-control disabled" id="service_id">
									<?php
									foreach ( $services as $key => $service ) {
										?>
                                        <option
                                                value="<?php echo( $service[ 'service_id' ] ); ?>" <?php
										if ( $service[ 'service_id' ] == $_SESSION[ 'post' ][ 'service_id' ] ) {
											echo( " selected" );
										} ?>><?php echo( $service[ 'service_description' ] ); ?></option>
										<?php
									}
									?>
                                    <!-- <option value="<?php echo( $_SESSION[ 'post' ][ 'service_id' ] ) ?>"><?php echo( $services[ $service_index ][ 'service_description' ] ) ?></option> -->
                                </select>
                            </div>


                            <div class="form-group">
                                <label for="problem_description" class="text-primary">Probleem omschrijving:</label><br>
                                <textarea name="problem_description" id="problem_description" class="form-control"
                                          rows="3"><?php echo( str_replace( '"', '', $_SESSION[ 'post' ][ 'problem_description' ] ) ); ?></textarea>
                            </div>

                            <div class="form-group">
                                <input type="hidden" name="user_id"
                                       value=<?php echo( $_SESSION[ 'user' ][ 'user_id' ] ); ?>>
                            </div>

                            <div class="form-group">
                                <input type="hidden" name="problem_id"
                                       value=<?php echo( $_SESSION[ 'post' ][ 'problem_id' ] ); ?>>
                            </div>

                            <div class="form-group">
                                <button type="submit" name="action" class="btn btn-primary btn-md rounded-pill" value="modify_problem">Opslaan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

		<?php
	} else {
		?>

        <div class="container">
            <div id="problem-row" class="row justify-content-center align-items-center">
                <div id="problem-column" class="col-md-6">
                    <div id="problem-box" class="col-md-12">
                        <form id="problem-form" class="form"
                              action=<?php echo( "'" . $_SESSION[ 'app' ][ 'homepage' ] . "'" ); ?> method="post">
                            <h3 class="text-center text-primary">Probleem maken</h3>


                            <div class="form-group">
                                <label for="reporter_email" class="text-primary">Email reporter:</label><br>
                                <input type="email" name="reporter_email" id="reporter_email" class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="reporter_f_name" class="text-primary">Voornaam reporter:</label><br>
                                <input type="text" name="reporter_f_name" id="reporter_f_name" class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="reporter_l_name" class="text-primary">Achternaam reporter:</label><br>
                                <input type="text" name="reporter_l_name" id="reporter_l_name" class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="service_id" class="text-primary">Stadsdienst:</label>
                                <select name="service_id" class="form-control" id="service_id">
									<?php
									foreach ( $services as $key => $service ) {
										?>
                                        <option
                                                value="<?php echo( $service[ 'service_id' ] ); ?>"><?php echo( $service[ 'service_description' ] ); ?></option>
										<?php
									}
									?>
                                </select>
                            </div>


                            <div class="form-group">
                                <label for="problem_description" class="text-primary">Probleem omschrijving:</label><br>
                                <textarea name="problem_description" id="problem_description" class="form-control"
                                          rows="3"></textarea>
                            </div>

                            <div class="form-group">
                                <input type="hidden" name="user_id"
                                       value=<?php echo( $_SESSION[ 'user' ][ 'user_id' ] ); ?>>
                            </div>
                            <div class="form-group">
                                <button type="submit" name="action" class="btn btn-primary btn-md rounded-pill" value="create_problem">Opslaan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

		<?php
	}
}
?>