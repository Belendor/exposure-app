<?php
/**
 * Created by PhpStorm.
 * User: barteld.logghe
 * Date: 17-7-2019
 * Time: 15:35
 */

class apicall
{
	private $url;
	private $HTTPHEADER;

	/**
	 * apicall constructor.
	 */
	public function __construct()
	{
		$filepath = __DIR__ . "/../../mapsi_ini/config.ini";
		$config = parse_ini_file( $filepath, true );
		$this->setHTTPHEADER( $config[ 'api' ][ 'HTTPHEADER' ] );
		$this->setUrl( $config[ 'api' ][ 'api-url' ] );
	}

	/**
	 * @param         $user_id
	 * @param         $function
	 * @param         $params
	 * @param boolean $logging
	 *
	 * @return string $response
	 */
	function call_api( $function, $user_id, $params, $logging )
	{
		$curl = curl_init();
		$url = $this->getUrl() . $function . "?mode=1&user_id=" . $user_id . $params;

		curl_setopt_array( $curl, array(
			CURLOPT_URL            => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING       => "",
			CURLOPT_MAXREDIRS      => 10,
			CURLOPT_TIMEOUT        => 30,
			CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST  => "GET",
			CURLOPT_POSTFIELDS     => "",
			CURLOPT_HTTPHEADER     => array(
				$this->getHTTPHEADER(),
				"Postman-Token: 08a5e57a-81c3-41da-9e3b-8b7f1bc81f75",
				"cache-control: no-cache",
			),
		) );

		$response = curl_exec( $curl );
		$err = curl_error( $curl );
		$httpcode = curl_getinfo( $curl, CURLINFO_HTTP_CODE );

		if ( !$logging ) {
			$_SESSION[ 'current' ][ 'calls' ][] = $url;
			$_SESSION[ 'current' ][ 'httpcode' ][] = $httpcode;
			$_SESSION[ 'current' ][ 'error' ][] = $httpcode;
		} else {
			$_SESSION[ 'current' ][ 'calls' ][] = $url;
			$_SESSION[ 'current' ][ 'httpcode' ][] = $httpcode;
			$_SESSION[ 'current' ][ 'error' ][] = $httpcode;


		}

		//var_dump($url);

		curl_close( $curl );

		if ( $err ) {
			echo "cURL Error #:" . $err;
		} else {

			return $response;
		}
	}

	/**
	 * @return mixed
	 */
	public function getUrl()
	{
		return $this->url;
	}

	/**
	 * @param mixed $url
	 */
	public function setUrl( $url )
	{
		$this->url = $url;
	}

	/**
	 * @return mixed
	 */
	public function getHTTPHEADER()
	{
		return $this->HTTPHEADER;
	}

	/**
	 * @param mixed $HTTPHEADER
	 */
	public function setHTTPHEADER( $HTTPHEADER )
	{
		$this->HTTPHEADER = $HTTPHEADER;
	}

}
