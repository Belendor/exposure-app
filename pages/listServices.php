<?php

/**
 * Created by PhpStorm.
 * User: barteld.logghe
 * Date: 8-8-2019
 * Time: 14:19
 */
/**
 * @var$apicall apicall
 */
$apicall = new apicall();
if ( $_POST[ 'action' ] == "create_service" ) {

	$params = "&service_description=" . urlencode( $_POST[ 'service_description' ] ) . "&service_email=" . urlencode( $_POST[ 'service_email' ] );
	$new_service = json_decode( $apicall->call_api( $_POST[ 'action' ], $_SESSION[ 'user' ][ 'user_id' ], $params, true ), true );
	$_SESSION[ 'current_return' ] = $new_service[ "return_status" ];

}
if ( $_POST[ 'action' ] == "modify_service" ) {
	//var_dump( "modify van service" );
	$params = "&service_id=" . urlencode( $_POST[ 'service_id' ] ) . "&service_description=" . urlencode( $_POST[ 'service_description' ] ) . "&service_email=" . urlencode( $_POST[ 'service_email' ] );
	$new_service = json_decode( $apicall->call_api( $_POST[ 'action' ], $_SESSION[ 'user' ][ 'user_id' ], $params, true ), true );
	$_SESSION[ 'current_return' ] = $new_service[ "return_status" ];

}
if ( !isset( $_POST ) ) {
	$services = json_decode( $apicall->call_api( "get_services", $_SESSION[ 'user' ][ 'user_id' ], "", true ), true )[ 'services' ];
}
?>
<?php
if ( $services ) {

	?>
    <form>
        <div class="form-row">

            <div class="form-group" class="col-md-2">
                <label for="reporter_email" class="badge badge-primary">Zoek service:</label><br>
                <input type="text" name="serviceNameFilter" onkeyup="filterService()" id="serviceNameFilter"
                       class="form-control">
            </div>
        </div>
    </form>
    <div id="data" class="jim-table-responsive">

    <table class="table table-striped"  id="serviceTable">
    <thead class="thead-dark" align="center">
    <tr>
        <th hidden="hidden" scope="col">dienst id</th>
        <th scope="col" onclick="sortTable(1,'serviceTable')">Dienst omschrijving</th>
        <th scope="col" onclick="sortTable(1,'serviceTable')">Dienst email</th>
    </tr>
    </thead>
	<?php
	foreach ( $services as $key => $service ) {
		?>
        <form action=<?php echo( "'" . $_SESSION[ 'app' ][ 'homepage' ] . "'" ); ?> method="post">
            <tr class="table-secondary" scope="row" onclick="
                    post(<?php echo( "'" . $_SESSION[ 'app' ][ 'homepage' ] . "'" ); ?>,{'action': 'Modify Service',
                    'user_id' :'<?php echo( $_SESSION[ 'user' ][ 'user_id' ] ); ?>',
                    'service_id':'<?php echo( $service[ 'service_id' ] ); ?>',
                    'service_description':'<?php echo( $service[ 'service_description' ] ); ?>',
                    'service_email':'<?php echo( $service[ 'service_email' ] ); ?>'
                    })">
                <td hidden="hidden"><?php echo( $service[ 'service_id' ] ); ?></td>
                <td data-label="Omschrijving"><?php echo( $service[ 'service_description' ] ); ?></td>
                <td data-label="Email"><?php echo( $service[ 'service_email' ] ); ?></td>
            </tr>
            <input type="hidden" name="user_id" value="<?php echo( $user[ 'user' ][ 'user_id' ] ); ?>"/>
            <input type="hidden" name="service_id" value="<?php echo( $service[ 'service_id' ] ); ?>"/>
            <input type="hidden" name="service_description"
                   value="<?php echo( $service[ 'service_description' ] ); ?>"/>
            <input type="hidden" name="service_email" value="<?php echo( $service[ 'service_email' ] ); ?>"/>
        </form>
		<?php
	}
	?>
    </table>
    </div>
    <?php
} else {
	?>
    no services found
	<?php
}
?>
    </table>
    </div>


<?php
?>