<?php
/**
 * Created by PhpStorm.
 * User: barteld.logghe
 * Date: 27-8-2019
 * Time: 12:07
 */
?>
<div id="login">
    <div class="container">
        <div id="login-row" class="row justify-content-center align-items-center">
            <div id="login-column" class="col-md-6">
                <div id="login-box" class="col-md-12">
                    <form id="login-form" class="form"
                          action=<?php echo( "'" . $_SESSION[ 'app' ][ 'homepage' ] . "'" ); ?> method="post">
                        <img class="rounded mx-auto d-block" src="https://i.ibb.co/vvDWW1R/logo1.jpg">
                        <div class="form-group">
                            <label for="user_email" class="text-primary">User email:</label><br>
                            <input type="email" name="user_email" id="user_email" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="password" class="text-primary">Password:</label><br>
                            <input type="password" name="password" id="password" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="remember-me" class="text-primary"><span>Remember me</span> <span><input
                                            id="remember-me" name="remember-me" type="checkbox"></span></label><br>
                            <button type="submit" name="action" class="btn btn-primary btn-md rounded-pill" value="login">Login</button>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12 control">
                                <div style="border-top: 1px solid#888; padding-top:15px; font-size:85%">
                                    Don't have an account!
                                    <a href="#" onClick="$('#login-row').hide(); $('#signup-row').show()">
                                        Sign Up Here
                                    </a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div id="signup-row" style="display: none" class="row justify-content-center align-items-center">
            <div id="login-column" class="col-md-6">
                <div id="login-box" class="col-md-12">
                    <form id="login-form" class="form"
                          action=<?php echo( "'" . $_SESSION[ 'app' ][ 'homepage' ] . "'" ); ?> method="post">
                        <img class="rounded mx-auto d-block" src="https://i.ibb.co/vvDWW1R/logo1.jpg">
                        <div class="form-group">
                            <label for="user_email" class="text-primary">User email:</label><br>
                            <input type="email" name="user_email" id="user_email" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="password" class="text-primary">Password:</label><br>
                            <input type="password" name="password" id="password" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="l_name" class="text-primary">Last name:</label><br>
                            <input type="text" name="l_name" id="password" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="f_name" class="text-primary">First name:</label><br>
                            <input type="text" name="f_name" id="f_name" class="form-control">
                        </div>
                        <div class="form-group text-center">
                            <label for="remember-me" class="text-primary"><span>Remember me</span> <span><input
                                            id="remember-me" name="remember-me" type="checkbox"></span></label><br>
                            <button type="submit" name="action" class="btn btn-primary btn-md rounded-pill" value="create_user" >Sign up</button>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12 control">
                                <div style="border-top: 1px solid#888; padding-top:15px; font-size:85%">
                                    Don't have an account!
                                    <a href="#" onClick="$('#signup-row').hide(); $('#login-row').show()">
                                        Sign Up Here
                                    </a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
