<?php
/**
 * Created by PhpStorm.
 * User: barteld.logghe
 * Date: 23-9-2019
 * Time: 8:25
 */
$dataPoints = array(
	array("label"=>"Chrome", "y"=>64.02),
	array("label"=>"Firefox", "y"=>12.55),
	array("label"=>"IE", "y"=>8.47),
	array("label"=>"Safari", "y"=>6.08),
	array("label"=>"Edge", "y"=>4.29),
	array("label"=>"Others", "y"=>4.59)
);
var_dump(json_encode($dataPoints));

?>
<script>
window.onload = function() {


var chart = new CanvasJS.Chart("chartContainer", {
theme: "light2",
animationEnabled: true,
title: {
text: "World Energy Consumption by Sector - 2012"
},
data: [{
type: "pie",
indexLabel: "{y}",
yValueFormatString: "#,##0.00\"%\"",
indexLabelPlacement: "inside",
indexLabelFontColor: "#36454F",
indexLabelFontSize: 18,
indexLabelFontWeight: "bolder",
showInLegend: true,
legendText: "{label}",
dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>
}]
});
chart.render();

}
</script>
<div id="chartContainer" style="height: 370px; width: 100%;"></div>
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
