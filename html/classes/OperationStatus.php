<?php
/**
 * Created by PhpStorm.
 * User: barteld.logghe
 * Date: 4-9-2019
 * Time: 14:23
 */

namespace classes;

class OperationStatus
{

	/**
	 * @var string
	 */
	public $state;
	/**
	 * @var string
	 */
	public $text;
	/**
	 * @var string
	 */
	public $info;

	/**
	 * OperationStatus constructor.
	 *
	 * @param $state
	 * @param $text
	 * @param $info
	 */
	public function __construct( $state, $text, $info )
	{
		$this->state = $state;
		$this->text = $text;
		$this->info = $info;
	}

	/**
	 * @return mixed
	 */
	public function getState()
	{
		return $this->state;
	}

	/**
	 * @param mixed $state
	 */
	public function setState( $state )
	{
		$this->state = $state;
	}

	/**
	 * @return mixed
	 */
	public function getText()
	{
		return $this->text;
	}

	/**
	 * @param mixed $text
	 */
	public function setText( $text )
	{
		$this->text = $text;
	}

	/**
	 * @return mixed
	 */
	public function getInfo()
	{
		return $this->info;
	}

	/**
	 * @param mixed $info
	 */
	public function setInfo( $info )
	{
		$this->info = $info;
	}


}