/**
 * Filter on both fields of service page
 */
function filterService() {
	let serviceFilter = document.getElementById( "serviceNameFilter" );
	let table = document.getElementById( "serviceTable" );
	let serviceFilterValue = serviceFilter.value;
	for ( var i = 0, row; row = table.rows[ i ]; i++ ) {
		//iterate through rows
		//rows would be accessed using the "row" variable assigned in the for loop
		//alert(row.cells[1].innerHTML + " = " + serviceFilterValue + " " + row.cells[2].innerHTML  + " = " + serviceFilterValue);
		if ( i !== 0 ) {
			if ( row.cells[ 1 ].innerHTML.includes( serviceFilterValue ) || row.cells[ 2 ].innerHTML.includes( serviceFilterValue )) {
				//alert( row.cells[ 1 ].innerHTML + " of " + row.cells[ 2 ].innerHTML + " = " + serviceFilterValue );
				row.style.display = '';
			} else {
				row.style.display = 'none';
			}
		}
	}
}


/**
 * Filter on problem page based on status and service
 */
function problemFilter() {
	let statusFilter = document.getElementById( "selectStatus" );
	let serviceFilter = document.getElementById( "selectService" );
	let table = document.getElementById( "problemTable" );
	let status_id = statusFilter.options[ statusFilter.selectedIndex ].value;
	let service_id = serviceFilter.options[ serviceFilter.selectedIndex ].value;
	let service_value = serviceFilter.options[ serviceFilter.selectedIndex ].text;

	for ( var i = 0, row; row = table.rows[ i ]; i++ ) {
		//iterate through rows
		//rows would be accessed using the "row" variable assigned in the for loop
		//alert(row.cells[0].innerHTML + " = " + service_id + "&" + row.cells[1].innerHTML + " = " + status_id);
		if ( i !== 0 ) {
			if ( ( service_id === "" || row.cells[ 0 ].innerHTML === service_id ) && ( status_id === "" || row.cells[ 1 ].innerHTML === status_id ) ) {
				row.style.display = '';
			} else {
				row.style.display = 'none';
			}
		}

	}

}


/**
 * sends a request to the specified url from a form. this will change the window location.
 * @param {string} path the path to send the post request to
 * @param {object} params the paramiters to add to the url
 * @param {string} [method=post] the method to use on the form
 */

function post( path, params, method = 'post' ) {

	// The rest of this code assumes you are not using a library.
	// It can be made less wordy if you use one.
	const form = document.createElement( 'form' );
	form.method = method;
	form.action = path;

	for ( const key in params ) {
		// alert(params[key]);
		if ( params.hasOwnProperty( key ) ) {
			const hiddenField = document.createElement( 'input' );
			hiddenField.type = 'hidden';
			hiddenField.name = key;
			hiddenField.value = params[ key ];

			form.appendChild( hiddenField );
		}
	}

	document.body.appendChild( form );
	form.submit();
}

/**
 * Sort table based on a click on a field of a table
 * @param n
 * @param tableName
 */
function sortTable( n, tableName ) {
	//alert(n);
	var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
	table = document.getElementById( tableName );
	switching = true;
	// Set the sorting direction to ascending:
	dir = "asc";
	/* Make a loop that will continue until
	 no switching has been done: */
	while ( switching ) {
		// Start by saying: no switching is done:
		switching = false;
		rows = table.rows;
		/* Loop through all table rows (except the
		 first, which contains table headers): */
		for ( i = 1; i < ( rows.length - 1 ); i++ ) {
			// Start by saying there should be no switching:
			shouldSwitch = false;
			/* Get the two elements you want to compare,
			 one from current row and one from the next: */
			x = rows[ i ].getElementsByTagName( "TD" )[ n ];
			y = rows[ i + 1 ].getElementsByTagName( "TD" )[ n ];
			/* Check if the two rows should switch place,
			 based on the direction, asc or desc: */
			if ( dir === "asc" ) {
				if ( x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase() ) {
					//alert(x.innerHTML.toLowerCase());
					// If so, mark as a switch and break the loop:
					shouldSwitch = true;
					break;
				}
			} else if ( dir === "desc" ) {
				if ( x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase() ) {
					// If so, mark as a switch and break the loop:
					shouldSwitch = true;
					break;
				}
			}
		}
		if ( shouldSwitch ) {
			/* If a switch has been marked, make the switch
			 and mark that a switch has been done: */
			rows[ i ].parentNode.insertBefore( rows[ i + 1 ], rows[ i ] );
			switching = true;
			// Each time a switch is done, increase this count by 1:
			switchcount++;
		} else {
			/* If no switching has been done AND the direction is "asc",
			 set the direction to "desc" and run the while loop again. */
			if ( switchcount === 0 && dir === "asc" ) {
				dir = "desc";
				switching = true;
			}
		}
	}
}