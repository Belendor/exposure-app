<?php
include_once "../pages/session.php";
require_once "../pages/apicall.php";
/**
 * @var$apicall apicall
 */

$apicall = new apicall();
include_once "../pages/list.php";

//var_dump($_SESSION);
//routing, decide what to include
if ( isset( $_SESSION[ 'post' ][ 'action' ] ) ) {
	$action = $_SESSION[ 'post' ][ 'action' ];
	switch ( $action ) {
		case "modify_service":
		case "create_service":
		case "List Services":
			include_once "../pages/listServices.php";
			break;
		case "List Problems":
		case "create_problem":
		case "modify_problem":
			include_once "../pages/listProblems.php";
			break;
		case "Create Problem":
		case "Modify Problem":
			include_once "../pages/createProblem.php";
			break;
		case "Create Service":
		case "Modify Service":
			include_once "../pages/createService.php";
			break;
		case "User info":
		case "create_user":
			include_once "../pages/userInfo.php";
			break;
		case "Graph2":
			include_once "../pages/graph2.php";
	}
}
//check if no user in session, go to login screen
if ( empty( $_SESSION[ "user" ] ) ) {

	include_once "../pages/login.php";
}

include_once "../pages/endList.php";

//remember calls and return value of current refresh, forget after refresh
$_SESSION[ 'api_calls' ] = $_SESSION[ 'current' ];
$_SESSION[ 'return' ] = $_SESSION[ 'current_return' ];
unset( $_SESSION[ 'current' ] );
unset( $_SESSION[ 'current_return' ] );

?>
